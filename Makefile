# Copyright 2023 Enzo Venturi. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

PREFIX=/usr/local
GOBIN=$(PREFIX)/bin

EXE=gs
GO=$(shell which go)
LDFLAGS=-w -s

all: fmt check test build

build:
	@echo "$(EXE) build options:"
	@echo "GO      = $(GO)      "
	@echo "LDFLAGS = $(LDFLAGS) "
	@echo "CGO     = DISABLED   "
	CGO_ENABLED=0 $(GO) build -ldflags="$(LDFLAGS)" -o $(EXE)

install:
	mkdir -p $(GOBIN)
	cp -pf $(EXE) $(GOBIN)

test:
	go test -v -count=1 ./...

bench:
	go test -bench=. ./...

fmt:
	gofmt -w -s .

check:
	test -z $(shell gofmt -l .)
	go vet ./$(DIR)
