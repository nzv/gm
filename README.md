     ___ _____ 
    | . |     |
    |_  |_|_|_|
    |___|      

Command `gm` is a git metrics tool

    go install -ldflags="-w -s" gitlab.com/nzv/gm@latest

For guides and reference materials about `gm`, see [the documentation][1].

[1]: https://pkg.go.dev/gitlab.com/nzv/gm
